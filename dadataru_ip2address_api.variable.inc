<?php

function dadataru_ip2address_api_variable_group_info() {
  $groups = [];

  $groups['dadataru_ip2address_api_connection_settings'] = array(
    'title' => t('Dadata.ru API Connection settings'),
    'access' => 'ip2address dadataru settings',
  );
  $groups['dadataru_ip2address_api_cache_settings'] = array(
    'title' => t('Dadata IP2Address database Cache settings'),
    'access' => 'ip2address dadataru settings',
  );

  return $groups;
}

function dadataru_ip2address_api_variable_info() {
  $variables = array();

  $variables['dadataru_ip2address_api_url'] = array(
    'title' => t('Server API callback url'),
    'group' => 'dadataru_ip2address_api_connection_settings',
    'access' => 'ip2address dadataru settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/detectAddressByIp',
    'type' => 'url',
    'token' => TRUE,
  );
  $variables['dadataru_ip2address_api_timeout'] = array(
    'title' => t('Connection timeout'),
    'group' => 'dadataru_ip2address_api_connection_settings',
    'access' => 'ip2address dadataru settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#field_suffix' => t('ms'),
    ),
    'type' => 'number',
    'default' => 250,
    'token' => TRUE,
  );
  $variables['dadataru_ip2address_api_token'] = array(
    'title' => t('API Token'),
    'group' => 'dadataru_ip2address_api_connection_settings',
    'access' => 'ip2address dadataru settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 42,
    ),
    'type' => 'string',
    'token' => TRUE,
  );

  $variables['dadataru_ip2address_api_cache_ttl'] = array(
    'title' => t('Database cache time to live'),
    'group' => 'dadataru_ip2address_api_cache_settings',
    'access' => 'ip2address dadataru settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 2,
      '#field_suffix' => 'days',
    ),
    'type' => 'number',
    'default' => 3,
    'token' => TRUE,
  );

  return $variables;
}
